# Russell's *The Principles of Mathematics* #

A PHP script to typeset Bertrand Russell’s *The Principles of Mathematics* in various formats.

The final product can be downloaded from [http://people.umass.edu/klement/pom/](http://people.umass.edu/klement/pom/).

### Requirements ###

* PHP 7+
* XeLaTeX (to create PDF versions)
* Calibre (to create ebook and plain text versions)
* Fonts used (though you can change these): OFL Sorts Mill Goudy, FreeSerif, Linux Libertine,XITS

### Usage ###

The master document is pom.php, which contains the book's texts along with functions which do different things for different formats for italics, mathematics, figures, chapter and sections, links, and so on.

By default the command:

`php pom.php`

will output HTML.

`php pom.php latex` or `php pom.php latex portrait` 

Will output LaTeX source (to be processed with XeLaTeX).

The ePub version is created by running Calibre's `ebook-convert`.

You can make all versions by running the script `make-all.php`.

### License ###

Russell's book in in the Public Domain.
This layout is released with a Creative Commons ShareAlike-Attribution 4.0 license.
This code GPLv3
Kevin C. Klement [klement@umass.edu](mailto:klement@umass.edu)
